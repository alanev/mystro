// modules
var gulp = require('gulp'),
	connect = require('gulp-connect'),
	
	// utils
	path = require('path'),
	plumber = require('gulp-plumber'),
	beep = require('./beep'),
	concat = require('gulp-concat'),
    connect = require('gulp-connect'),
	
	// js
	webpack = require('webpack'),
    glob = require('glob')
	;

// paths
var paths = require('./paths');;

// task
var tasks = function () {
    var files = {};
    glob.sync(
            path.join(
                process.cwd(), `${paths.src}*.js`
            )
        ).forEach(function (p) {
            files[path.parse(p).base] = p;
        });
    
	webpack({
        entry: files,
        output: {
            filename: '[name]',
            path: path.join(process.cwd(), paths.dest, '/assets/js')
        },
        resolve: {
            root: [
                path.join(process.cwd(), paths.modules)
            ]
        },
        module: {
            loaders: [{
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }]
        },
        plugins: [
            new webpack.optimize.UglifyJsPlugin()
        ]
	}, function (err, stats) {
        if (!err) {
            gulp.src(path.join(paths.dest, `assets/js/*.js`))
                .pipe(connect.reload())
        } else {
            console.log('Webpack error:', err);
        }
    });
};

// module
module.exports = tasks;