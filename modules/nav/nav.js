module.exports = {
    // article: document.querySelector('.article'),
    bar: document.querySelector('.nav__bar'),
    toggleElement: document.querySelector('.nav__toggle'),
    content: document.querySelector('.g-page'),
    close: document.querySelector('.nav__close'),
    activeClass: '_active',
    listen ({
            // options
            pageY = window.pageYOffset
        }) {
            
            var showPoint = 400;
            if (window.outerHeight / 2 < showPoint) showPoint = window.outerHeight / 2;
            
            // body
            if (pageY > showPoint) {
                this.bar.classList.add(this.activeClass);
            } else {
                this.bar.classList.remove(this.activeClass);
            }
    },
    toggleMenu () {
        this.content.classList.toggle('_side_open');
    },
    init () {
        if (this.bar) {
            var that = this;
            window.addEventListener('scroll', e => that.listen(e));
            that.toggleElement.addEventListener('click', e => that.toggleMenu());
            that.close.addEventListener('click', e => that.toggleMenu())
        }
    }
};