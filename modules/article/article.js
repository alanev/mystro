module.exports = {
    element: document.querySelector('.article__status'),
    value: document.querySelector('.article__status-value'),
    bar: document.querySelector('.article__status-read'),
    listen ({
            // options
            pageY = window.pageYOffset
        }) {
            // body
            let parent = document.querySelector('.article__content');
            let value = ((pageY - parent.offsetTop) / parent.offsetHeight * 100).toFixed(0);
            
            if (pageY > parent.offsetTop && value <= 100) {
                this.element.classList.add('_active');
                this.bar.style.width = `${value}%`;
            } else {
                this.element.classList.remove('_active');
                value = 100;
            }
            if (pageY < parent.offsetTop) value = 0;
            
            this.value.innerHTML = value;
    },
    init () {
        if (this.element) {
            let that = this;
            window.addEventListener('scroll', e => that.listen(e))
        }
    }
};