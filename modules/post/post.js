const className = '_hover';

module.exports = {
  elements: document.querySelectorAll('.post__preview'),
  body: document.querySelector('body'),
  hoverClass: '_hover',
  touchstart () {
    this.elements.forEach(elem => elem.classList.remove(className));
  },
  touchend () {
    this.classList.add(className)
  },
  init () {
    var that = this;
    if (this.elements) {
        window.addEventListener('touchstart', e => that.touchstart(e));
        that.elements.forEach(elem => elem.addEventListener('touchend', that.touchend));
    }
  }
};