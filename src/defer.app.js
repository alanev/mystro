NodeList.prototype.forEach = Array.prototype.forEach;

// var header = require('header/header.js');
var article = require('article/article.js');
var nav = require('nav/nav.js');
var post = require('post/post.js');

// header.init();
article.init();
nav.init();
post.init();